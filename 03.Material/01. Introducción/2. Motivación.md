# Motivación

Hay muchos términos dando vuelta

- Data mining
- Big data
- Pattern recognition
- KDD
- Machine learning
- Artificial intelligence
- Statistics
- Data analytics
- Neural Netowrks
- Data science
- Deep learning

Mucho ruido. Machine learning es un subconjunto de Artificial intelligence (Aprendizaje automático - Inteligencia artificial)

	Artifical intelligence > Machine learning > Deep learning

De 1998 de acá para adelante va creciendo mucho. Porque? Poder de computo desbloqueado, muchos datos. Redes neuronales vuelven. Se desarrollan muchisimo. 

Trending topis:

- Redes neuronales
- Deep network
- Tensorflow
- NLP

En el último tiempo:

- Mujeres publicando
- Ética e IA (Perspectiva racial también)


## Tipos de IA

* Simbólica: Compreender el dominio del problema y generar un modelo (En los '80, cayó en desuso)
	* Sistemas basados en agentes
	* Sistemas basados en reglas
* No simbólica: Datos para generar un modelo
	* Aprendizaje automático
	* Minería de datos

En el útlimo tiempo, incluso se hace el feature extraction con redes neuronales. 

## Interpretabilidad

* Modelos de caja blanca
	* Se pueden inspeccionar, ver los pesos, ver como resuelve
	* Ej: Regresión lineal, modelos probabilísticoss
* Modelos de caja negra
	* No es posible interpretar el modelo.
	* Van surgiendo formas, pero es dificil/'imposible' entender como toma la decisión, que prioriza, elije el modelo para tomar la decisión
# Data augmentation

Generar nuevos ejemplos con transformaciones útiles (Rotacion, agregar ruido, pixelar la imagen). Transformaciones que no se metan con mi problema. Si quiero clasificar, rotar las imagenes no es lío.

* Rotar en -30 y 30
* Crop
* Flip (Espejarlas)

Tip práctico -> Clase _ImageDataGenerator_. Se le agregan las transformaciones que queremos.

ImageDataGenerator también puede hacer transformaciones entrenables. Por ejemplo la normalización.

## En texto

Se pueden hacer, pero es más dificil. 

* Reemplazar palabrasp or sinónimo
* Insertar palabras
* Sustituir palabras/frases
* Borrar palabras
* Colocar palabras mal escritas (Lo mencionamos en clase, será?)


# Salvar y cargar modelos

* Modelo completo
	* Model.save
	* Pesos y definición
* Solo los pesos
	* Save_weights
	* Solo los pesos, pero no la arquitectura. Útil para cambiar de framework, pero hay que reconstruir la arquitectura.

Extension _h5_ según el video. Formato HDF5. Diseñado y optimizado para datos numéricos.


# Modelos de grafos arbitrarios

Clase _model_ permite crear un grafo arbitrario de capas. Mucho más flexible. 


# Optimización de hiperparámetros

_Hiperparámetros_: Por fuera del modelo

* Cantidad de capas
* Tipo de capas
* Optimizador:
	* Tasa de aprendizaje
	* Regularización
	* Función de error
* Cantidad de épocas
* Data augmentation
* Inicialización de parámetros

* Se optimizan 'a mano'

_Parámetros_: están dentro del modelo
* Pesos de filtros convolucionales
* Sesgos / Bias
* Pesos de las capas lineales
* Se optimizan por descenso de gradiente

## Como optimizamos?

* Fuerza bruta: Problema de poder computo. Explosión combinatoria de opciones.
* Búsqueda en grilla: Fuerza bruta pero con límites.
* Búsqueda aleatoria: Elegir k combinaciones aleatorias de hiperparámetros.

Se puede hacer con subsets, para hacer búsqueda de hiperparámetros. No son tan sensibles a la cantidad de datos, mas bien a la naturaleza del problema.


# Modelos pre-entrenados

Transferencia de aprendizaje y finetuning.

Como entrenar un modelo requiere mucho tiempo/uso de CPU, optimización de hiperparámetros, preparación de datos. Podemos tomar modelos ya entrenados y tomarlos como base para lo que queramos hacer. 

Un modelo pre-entrenado es más fácil y rápido para empezar. Pero tiene un dominio y arquitectura fija, lo cuál lo hace menos flexible. (Igual los modelos interesantes para traernos son aquellos que fueron entrenados con grandes cantidades de datos en problemas complejos. Tipo palabras, imágenes)

## Modelos convolucionales

Suelen ser muy buenos para reutilizar. Aprenden filtros generales. 

Podemos reentrenar la red, 

* Sacando la última capa Dense o GlobalAveragePooling.
* Congelando las capas anteriores 
	* Congelando significa no reentrenar los pesos. No se tocan.
	* Pero podría elegir las que quieras
* Luego agrego mi propia última capa, y si la entreno

## Finetuning

Tomo un modelo anterior y reentreno toda la red.

El problema para hacer esto es tener poder de computo. Va a ser un red probablemente grande, con muchos parametros, y tengo que reentrenar todo. 


# Ensamble de modelos

Nos permite hacer un metamodelo combinando varios modelos. Combinar con votación, promedio. 


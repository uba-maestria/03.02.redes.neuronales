# Regresión logística

## Clasificación binaria y prediccion de probabilidades

Si mi resultado es una probabilidad puedo hacer una regresión, pero muchas veces tengo una clasificación binaria (Si o no).

## Regresión Logistica

Regresión logistica = Regresión lineal + Función logística

Básicamente le aplico la función logística a la salida de la regresión lineal.

### Función logistica

* Función: 1 / ( 1 + e ^ -x )
* Su derivada es simple
* El dominio es de -inf a +inf
* Imagen / valor y => [0, 1]

### Regresión logistica

La formula sería: **1 / ( 1 + e ^ -( m x + b ) )**

* M le cambia la prendiente a la parte lineal de la logistica. 
	* M = 1 => La pendiente es tipo 45°
	* M >> 1 => Voy a un modelo if/else. En un umbral pasa.
	* M más cercano a cero, se plancha la pendiente
	* Si M es negativo, lo mismo pero invertido.
* B le cambia la ordenada al origen

(Minuto 13 del video 02, hace una referencia visual, muy buena)

En una dimensión, tengo un punto de corte. Una frontera de dimensión. En dos dimensiones, es una recta, una regresión.

## Runción de error - entropía cruzada

Error cuadrátrico medio (ECM) funciona, pero no es una función convexa. Por no es tan fácil optimizar. No es tan óptimo para una función de probabilidad. Entonces usamos **Entropía cruzada**. 

* E = 1 / ( n * Sum(E_i) )
	* E_i = -log ( sigma( m * x_i + b ) )  // Si y_i = 1
	* E_i = -log (1 - sigma( m * x_i + b ) )  // Si y_i = o
	* Penaliza si estás mas lejos del resultado.
	* Esto se puede vectorizar para los calculos. 
	* (Minuto 5 del video 3)

* Las derivadas de la entropía cruzada son iguales que las de regresión lineal, mismas ecuaciones de descenso de gradiente. _Eso es muy bueno_.


### Interpretación

ECM mide distancia entre punto, usando la distancia euclídea al cuadrado.

Entropía cruzada mide la distancia entre distribuciones de proabilidad. Usa la distancia Kullback-leiber (o Divergencia)

Me da valores entre 0 y +inf. Eso si es un problema de interpretación. Ahí es bueno usar otras métricas, por ejemplo _accuracy_ (Cantidad de veces que acertó)


### Regresión logística múltiple

* Multiples opciones => Predicciones independientes
* Clases mutuamente exclusivas => Problema de clasificación

Podemos generarlos labels de salida como: "one-hot encoding" o como "etiquetas"

Usamos regresión lineal con varias salidas en lugar de una. Ahí en lugar de la sigmoidea, usamos la función "Softmax". La función softmax aplicada a la salida genera una distribución de probabidad de P(Y) => y con eso clasificar N clases.

La función softmax, 
* Devuelve una distribución de probabilidades para los valores de salida. 
	* Los resultados suman 1
* Es exponencial, acentúa los valores altos
* El resultado es > 0

Usamos entropia cruzada => -ln (P[clase_verdadera])

## Metricas


Matriz de confusión -> Diagonal lo correcto
* Clasificacion binaria
* Clasificación multiclase
* TN, TP -> Correctos, en la diagonal
* FN, FP -> Equivocos

Cosas que puedo calcular desde la matriz
* Accuracy, cantidad de correctos => TP + TN / Todos
	* Problema en dataset desbalanceado, elije la clase mayoritaria, y le pega la mayoría de las veces.
* Precision => TP / (TP+FP)
* Recall => TP / (TP + FN)
* f_1 = 2 * [( Precision * recall) / (precision + recall)]

Curvas precisión-racall. Se computan de igual modo que las curvas ROC, pero grafican el funcionamiento del modelo para estás métricas.
Curvas roc => True positive rate (Recall) vs False positive rate (1 - especificidad)
# Redes convolucionales

## Imagenes digitales

Una imagen es una matriz de N x M pixeles ( Resolución ). A mayor resolución, mejor definición.

Pixel:
* Unidad de información más pequeña en una imagen digital
* Es un punto, de un color particular
* Representa la intensidad de la luz en ese punto.

### Como encodeamos los colores?

* Si fueramos por 0/1 => Tendriamos blanco/negro puro.
	* 1 bit = 2 colores
* Si vamos por un rango, podríamos poner los intermedio entre blanco y negro.
	* 2 bits = 4 colores
	* 8 bits = 256 colores
* A mayor rango de valores posibles, mayor cantidad de colores (**profundidad de color**)
	* Se suelen usar 256 colores, y varios canales.
	* RGB - YCMK - HCV (Hue saturation value) (Distintos encodings)
* RGB => Tiene tres canales, un pixel puede tener 256 valores en cada canal.
	* Sistema aditivo. Con Rojo verde y azul podemos armar cualquier otro color.
	* Nosotros naturalmente tenemos más receptores para el verde.
	* True color is an RGB color model standard, usa 24 bits para almacenar la información.
		* 224 gives 16,777,216 color variations. The human eye can discriminate up to ten million colors,[11] and since the gamut of a display is smaller than the range of human vision, this means this should cover that range with more detail than can be perceived.
		* Versiones más nuevas, tiene 32 bits e incluyen la transparencia.
	* 0 => Negro; 255 => Mayor color. 
	* (255, 255, 255) => Blanco 
	* RGB a Grises 
		* => ( R + G + B ) / 3
		* Como tenemos más verde => ( 30 * R + 60 * G + 10 * B ) / 3 


Para redes neuronales es interesante usar float y no int. Pero es la misma lógica.

### Histogramas

Cuantos pixeles de cada color. Útil para tener un pantallazo de la imagen y la distribución de los colores.


## Clasificación de imágenes

Primer tema, las imagenes son matrices, y las redes no toman matrices => Necesito aplanar la imagen (np.reshape). Por lo menos pasa en las redes normales.

Algunos datasets de ejemplo
* MNIST -> 5000 ejemplos de cada numero, escrito manuscrito
* CIFAR10 -> Ejemplos de imagenes varias

Algo útil es visualizar los ejemplos mal calificados para intentar entender por donde puede estar fallando.


## Filtros convolucionales

Operación sobre dos funciones f y g, que produce una tercera función que puede ser interpretada como una versión “filtrada” de f.

En funciones unidimensionales se utiliza para realizar diferentes filtros en señales o modelar estímulos en simulaciones. 

Si bien la convolución se define en forma continua, a nosotros nos interesa la versión discreta.

### Filtro discreto 

El filtro es una matriz de valores. Este filtro se llama también **kernel**/nucleo y tiene un parametro **kernel size**.

Básicamente va recorriendo la imagen y ponderandola, con eso genera la salida.

Uno puede imaginarlo con distinta dimensionalidad:
* Vector
* Una matriz (Util para un único canal)
* Una matriz n-dimensional => Útil para varios canales 

### Parametros

* Kernel size -> Tamaño de la matriz
	* Suele ser: Cuadrado e impar (3x3, 5x5, 7x7).
* Padding -> Espacio que se agrega en los bordes
* Stride -> Salto de la matriz. Va recorriendo la imagen un pixel a la vez, o se saltea?

### Algunos filtros conocidos

* Promedio de los pixeles -> Blur
* Gaussiano -> Pondera el centro. Filtra alto detalle
* Detección de borde -> 
	* El centro fuerte, alrededor muy bajo. Realza los detalles
		* -1, -1, -1
		* -1,  8, -1
		* -1, -1, -1
	* Bordes horizontales: Por ejemplo útil para detectar ojos.
		* -1, -1, -1
		*  2,  2,  2
		* -1, -1, -1
	* Bordes verticales
		* -1, 2, -1
		* -1, 2, -1
		* -1, 2, -1

Podemos concatenar filtros y detectar ciertas cosas.

Mayor tamaño del kernel, más potente es el efecto. 

Si concatenamos el mismo kernel, se amplifica también.

El convolucional aplana. Si quisieramos hace una salida de dos dimensiones, toca poner dos convoluciones. Cada una funciona para si misma, más parametros, más entrenamiento. (El mismo tema que poner n regresiones)

### Tamaños de las salidas

* Kernel  => K  x K
	* (El kernel de K x K, va a tener k * k hiperaparámetros que tengo que aprender)
* Input   => H  x W
* Output  => H' x W'
* Padding => P
* Stride  => S
	* Stride = 2, me divide la imagen a la mitad. Se saltea un pixel en cada paso.

Sin padding

H' = H - (K - 1)
W' = W - (K - 1)

Con padding

H' = H + 2P - (K - 1)
W' = W + 2P - (K - 1)


## Redes convolucionales

En keras => Conv2D

* Kernel_size
* Strides = (n,m)
* Activation ( Relu, TanH, ...)
* Padding

### Capas pooling

Ayudan a reducir la dimensionalidad espacial del feature map. 

Básicamente son convoluciones con un stride igual al kernel y donde se calcula alguna función sobre todos los pixeles:

* Maximo
* Minimo
* Promedio

Reduce la dimensionalidad y ayudan en la clasificación.

Un clasico es el _max pooling_, toma el mayor valor de la convolución.

Un ejemplo de arquitectura:
* Convolution + nonlinearity
* Max pooling
* Fully connected layers
* Nx binary classification
 
### Capas convolucionales

Lo más usual es tener kernels: 1x1, 3x3, 5x5

El modo más ordenado de realizar una arquitectura **ConvNet** es intercalar capas Convolucionales con capas Pooling hasta llegar a las capas Dense (Feed-Forward) que discriminarán las características aprendidas por las capas anteriores.


## Visualización de filtros

Keras nos permite consultar las capas y ver información relevante:
* Capas, features, entradas y salidas
* Tamaño
* Visualizar los filtros y resultados intermedios (Que valora de la imagen)
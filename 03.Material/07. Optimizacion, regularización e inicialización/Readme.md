# Variantes de descenso de gradiente

## Momentum

Arrancamos con descenso de gradiente tradicional. Despues descenso de gradiente estocástico (Con azar). (Redes son casos no convexos, con varios mínimos)

Una mejora es agregar un **momentum**, un proxy de la direccion y la fuerza con la que venía optimizando. Direccion y velocidad promedio. La _ventaja_ es que nos ayuda a escapar de los mínimos locales "malos".

## Optimizadores avanzados

* Adagrad
	* Normaliza la magnitud de los gradientes
	* Importa más la dirección
* ADAM
	* AdaGrad + Momentum
	* alfa/α no es tan importante
	* Algoritmo por defecto cuando no hay tiempo (No tengo que elegir el alfa)
	* _útil para prototipar_

Los algoritmos más avanzados requieren menos pasos, pero cada paso es más caro.

Los modelos entrenados son ligeramente peores que con SGD (Estocástico). Porque acá hay aleatoriedad.


# Inicialización de parámetros

En redes, es más dificil de optimizar. Los parámetros iniciales pueden cambiar rotundamente el resultado.

Hay zonas con derivadas muy pequeñas (nos vamos a mover muy poco), otras con derivadas muy grandes (vamos a movernos mucho, con brusquedad), y otras más 'normales'.

## Como inicializamos?

- En cero? No sirve. Las derivadas son 0, predice 0. Ergo, no aprende.
- Otro valor 5? Tampoco sirve. Hay simetría y todas las neuronas predicen lo mismo.

Lo mejor es la **aleatoriedad**. 

* Distribución normal o gaussiana
	* Tengo que elegir media y varianza
* Distribución uniforme
	* Elegir un rango de hiperparámetros (a,b)

Si no funciona. Podríamos re-inicializar y volver a entrenar.	

## Inicializadores avanzados

Inicializador de Glorot para TanH

El objetivo es encontrar magnitudes de los valores intermedios estables

* _Estable_: No tiende a 0, ni a valores muy grandes.
* Glorot es para capas con TanH
	* Distribución Normal 
	* Media 0
	* Varianza = 2 / ( #inputs + #outputs )
	* Inputs => Neuronas de la capa anterior
	* Outputs => Neuronas de la capa siguiente

* He es para capas con ReLu
	* Distribución Normal 
	* Media 0
	* Varianza = 2 / #inputs
	* Inputs => Neuronas de la capa anterior

De está forma, las activaciones, los gradientes, los pesos, quedan más estables. Son más parecidos, no se dispersan tanto, quedan en un mismo orden de magnitud.

Keras tiene inicializadores por defecto. En la clase _"Dense"_ y en la clase _"Conv2D"_ hay un parametro de kernel_initializer (inicializador de pesos) y bias_initializer (inicializador de sesgos). Por defecto usa 'glorot_uniform', se puede usar 'he_uniform' / 'he_normal'-


# Regularizacion de pesos

La regularización es otra herramienta que tenemos para disminuir la complejidad del modelo (Una especie de puja entre complejizo y lo simplifico. Tipo crece el arbol y lo podo).

## Regularización L2 de pesos

Hago una función de error compuesta:

E = Sum(E_i) + lambda * Sum( p_i ^ 2 )

p_i^2 penaliza a pesos grandes (W). A las predicciones extremas, que son las que marcan el sobreajuste. Si el peso, p_j = 0 => es lo mismo a que la neurona no esté.

Nuestro parámetro es lambda. Si lambda es muy chico, no juega un papel. Si lambda es muy grande, te plancha, no le va a importar el error, solo el ajuste. Hay que buscar el lambda optimo, intermedio.

## Tipos de regularización

* Norma Euclídea (L2)
	* Penalización cuadrática. 
	* p^2
	* ||p||^2 si es un vector.
* Norma Manhattan (L1)
	* Penalización lineal
	* abs( p ) 
	* sum( | p_i | ) , si es vector
	* No es derivable en 0, pero funciona. Es menos fuerte que L2.

## Que parámetros penalizar?

* Pesos w de Dense o Conv
	* **Si**, w multiplica a entrada x
	* Genera valores extremos
* Sesgos b de Dense o Conv
	* **No**, sólo suman un valor a la salida
	* Permiten ajustar a la escala de datos

## Regularización y descenso de gradiente

El descenso de gradiente va a buscar minimizar el error y la penalización. Le saca fuerza al error, que es lo que ayuda a no sobre-entrenar.

## Weight decay

Pesos que decaen. 

El objetivo bajar elvalor de los pesos. Es un paso extra en descenso de gradiente.

Calculo un gradiente sobre los pesos, y los ajustando también.

## En Keras

Parámetro kernel_regularizer = regularizers.l1_l2(l1=, l2=)
Parámetro kernel_regularizer = regularizers.l2(l2=)
Parámetro kernel_regularizer = regularizers.l1(l1=)


# Carga de datos

Usar generators. Crea un iterador.


# Mejoras de modelos

Como se puede mejorar un modelo:

* Rediseñarlo
	* Requiere
		* conocimiento
		* Inteligencia
		* Mucho tiempo/trabajo
* Poner más datos
	* Debería ser la primera estrategia a probar
	* Articulos
		* => "The unreasonable effectiveness of mathematics in the natural science"
		* => "The unreasonable effectiveness of data"
		* => "How much data is enough?"
	* Problemas:
		* Etiquetar datos es "caros"
	* Opciones:
		* Hacer un crawler, bajar imágenes, etiquetarlas a mano
			* Buenos resultados
			* Mucho tiempo de etiquetado y verificación (Surge el crowdsourcing de etiquetas)
			* Generalmente quien puede pagarlo es una compañía
		* Data augmentation
			* Multiplicar los datos existentes.
			* Hacer transformaciones, que no afecten al trabajo, sobre los datos para generar nuevos
			* Girar imagenes, cambiarle la tonalidad o el ruido (Si no afecta)
			* Cambiarle le modularidad a la voz, cambiar la velocidad, subirlo o bajarle una octava (Siempre que no afecte)
* Mejorar la arquitectura
	* Hacer varias arquitecturas y compararlas
	* Entender los errores y diseñar solcuones
	* Evolución de modelos
		* VGG -> Convoluciones 3x3
		* AllConvolutional -> GlobalAveragePooling, sin maxpooling o densas
		* MobileNets -> Bloque con convoluciones separables depthwise
		* Inception -> Bloques inception (Representación más ricas)
	* Buscar en literatura, los grupos de investigación están corriendo las fronteras constantemente
	* Saber que no hay una arquitectura correcta, sino que son varias las que funcionan
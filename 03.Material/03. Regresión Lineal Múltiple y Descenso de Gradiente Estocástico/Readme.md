# Regresión Lineal con Múltiples Variables

Más información, más dimensiones.

* Modelo: hiperplano en R^m
* m variables = coeficientes w
* f(x) = x_1 * w_1 + x_2 * w_2 + ... + x_m * w_m  
	* En vectores:
		* Vector w => Coeficientes/Pesos relativos de cada variable. Pesos negativos => Relaciones negativas.
		* Vector x => observaciones/variables
		* Vector b => ordenada al origen, término constante
		* f = w . x + b

Más dimensiones, no se puede graficar, más dificil de 'adivinar/percibir'. Toca ir por métodos analíticos: derivadas, descenso de gradiente, vectores/producto escalar.


## Interpretación del modelo

Regresión Lineal

* Podemos interpretar coeficientes
	* Modelo de “caja blanca”
* Escala de coeficientes depende de escala de variables 
	* Normalizar variables hace a los coeficientes comparables entre variables

## Nomenclaturas

* Regresión lineal simple
	* 1 variable de entrada
	* 1 variable de salida
* Regresión lineal 
	* m variable de entrada
	* 1 variable de salida
* Regresión lineal múltiple
	* m variable de entrada
	* k variable de salida

## Regresión lineal múltiple

* Predecir K valores a partir de M valores
* Transformar un vector en otro
	* Es una idea muy potente y flexible. Es lo que está detras de las redes neuronales, texto, imagen, ...
* Modelo: X . W + B
	* Vectores/Matrices (Depende de m)
	* Producto entre matrices
* Error => Distancia euclidea al cuadrado

## Recursos para repasar operaciones vectoriales

* Producto punto o escalar entre dos vectores:
	* https://www.youtube.com/watch?v=N5f7pYTNcFM
* Multiplicación de un vector por un escalar:
	* https://es.khanacademy.org/math/algebra-home/alg-vectors/algscalar-multiplication/v/understanding-multiplying-vectors-by-scalars
* Multiplicación de una matriz por un escalar:
	* https://www.youtube.com/watch?v=-ArUqjhQIBM
* Producto de una matriz y un vector
	* https://www.youtube.com/watch?v=2Gdy1xRnqjk
* Producto de una matriz y otra matriz
	* https://www.youtube.com/watch?v=Tjrm3HsqBXE
	* https://es.khanacademy.org/math/linear-algebra/matrix-transformations/composition-of-transformations/v/linear-algebra-matrix-product-examples

(TODO: En el video ya cambiaron los nombre de los vectores. Lo que era M variables es N, lo que eran K salidas es M)

## Descenso de Gradiente Estocástico o por lotes

El descenso de gradiente normal tiene un costo => 

* Por iteración:
	* Derivada del error por los pesos tiene => O ( N x P ) donde N son los ejemplos y P los parámetros
	* Actualizacion de los pesos => O ( P )
* El costo total es => O ( I x N x P ) + O ( I x P )
	* I = Iteraciones

Una opción es derivar el total de datos de entrenamientos en lotes. Ahí voy a tener una derivada ruidosa, un gradiente aproximado, pero lo suficientemente bueno y con un costo computacional bien más chico. 

* Lotes = Epochs = Epocas
	* _Descenso de gradiente estocástico_
* B = Cantidad de lotes
	* Si B = N => Tengo el descenso de gradiente clásico ( El batch incluye a todo el batch )
	* Si B = 1 => Tomo de a 1 es demasiado ruidoso ( El batch incluye un solo ejemplo ) 
* En cada iteración calculo derivadas para cada conjunto de datos. ( Costo => O ( B x P ) )

Hay una función distinta que me calcula la derivada para un conjunto de datos. 

* Costo total => O ( I x B x P )  

### Comparación

* El descenso de gradiente clásico 
	* Usa todas las muestras. 
	* El gradiente es exacto (Respecto de las muestras), ruidoso respecto de la distribución
* El descenso de gradiente estocástico
	* Utiliza una muestra de una muestra
	* El gradiente no es exacto, pero es útil si tomamos un B adecuado
	* Es útil para entrenar

### Por qué funciona?

Con los datos, que son una muestra, estimo la distribución. Nunca es la verdadera => Si nos apegaramos mucho a los datos aprenderiamos de una distribución no exacta. 

Como la función de error no la puedo conocer (Porque no conozco la distribución real). Pero con la función de error de cada lote, va a aproximarse. 

### Tamaños

* Iteraciones reales por epoch => N / Batch_size
* Iteraciones reales totales => ( N / Batch_size ) * Epochs

Como lo elijo? 

Suele ser una potencia de 2 ( 8, 16, 32, 64, 128, 256, 512). Más de 512 no suma. 

Tengo una limitación de memoria. Según la cantidad de memoria que tenga voy a poder alocar iteraciones.

Tan grande como sea posible siempre que entre en la RAM.

### Mejoras

Ayuda al entrenamiento

* Generar ruido. Si le meto aleatoriedad, mejora el entrenamiento.
	* Desordenar los lotes en cada epoca
	* Desordenar los datos que entran en cada batch, en cada epoca
* La GPU mejora la performance para entrenar}}	

## Keras

* Deep neural network library
* Objetivo facilidad de uso
* Prototipado rápido
* Modelos pre entrenados
* Python
* Programar a nivel de capas
* Backend es tensorflow por defecto (Implementa más eficiente)

Vs Tensorflow es mucho menos verboso, pero mucho menos flexible. Tensorflow es lazy/declarativo, no ejecuta cuando voy escribiendo. Se crea el grafo, pero hasta no ponerle el 'run', no se ejecuta. Es en C++.
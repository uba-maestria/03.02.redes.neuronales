# Redes neuronales

## Límites de modelos lineales

### Regresión Logística y Lineal Múltiples

* Herramientas simples pero poderosas.
* Permiten
	* Transformaciones arbitrarias entre vectores (lineal).
	* Dar estructura de distribución de probabilidad a la salida (logística).
* Lo primero a intentar en cualquier problema
* Ventajas
	* Caja blanca, interpretable
	* Error convexo, solución única, fácil de entrenar.
* Desventajas
	* No pueden resolver problemas no-lineales (Limites difusos, limites no lineales/curvos, XOR)

### XOR

Son dos clases, pero no pueden separarse linealmente. No se soluciona con un hiper-plano. (Minuto 5:30 del video 01. La visualización está piola)

Posible solución: Dos regresiones / Dos hiperplanos -> Con eso reclasificamos los ejemplos -> Regresión logistica.

Combinar varias transformaciones
* Aumenta el poder de los modelos
* A eso llamamos "_Redes Neuronales_"
* Resuelvo los problema no lineal(mente separable)


## Redes neuronales secuenciales

Idea básica _Apilar transformaciones_ (Transformación = Capa)

	X -> Regresión 1 -> Regresión 2 -> Regresión 3 -> Y

Podría tener redes secuenciales:
* Lineal
* Ciclicas
	* Agregan el concepto del tiempo. Los autoencoders, y redes que se usan para texto, usan está estructuras
* Aciclicas
	* Paralelizan pero sin bucles

_Forward_: Cálculo de la salida de la red

**Importante**: Tiene que matchear la entrada de la red con los parametros, y tiene que matchear la salida con lo que se espera. (En el medio tengo libertad para combinar)

* La cantidad de capas son arbitrarias. Son hiperparámetros que tenemos que decidir.
	* Siempre tener en cuenta parametros de entrada y salida
	* La última capa se llama head o cabeza. Modifica el dato de salida (Por ejemplo si agrego una regresión logistica devuelvo probabilidad)
	* Capa densa o fully connected => Regresión lineal (Patrón denso, todo vinculado con todo)
* Si bien podríamos generalizar cualquier problema con una sola capa / dos capas (Teorema de Cybenko). Más capas es mejor, porque hace que sea más rápido de entrenar.
* Las capas son reutilizables. 

### Función de error

* Considerarla una capa más
	* Capa especial
	* Pero esa capa recibe el output y el valor real. Con eso va a poder calcular por cuanto le erro.
* No se usa para predecir  (Solo en entrenamiento)


### Funciones de activación

Si no hay funciones de activación => es lo mismo que tener una regresión lineal. Lo que hace que salgamos de la linealidad es poner funciones de activación. 

* _Funciones populares_: 
	* ReLU(x) -> 
		* < 0 => da 0
		* > 0 => la función identidad
		* Tiende a usarse ReLU (Para los negativos 0, para los positivos lineal)
	* Tanh(x) -> Parecida a Sig. Con mas pendiente, y corrida en -1.
	* Sig(x) -> Sigmoidea, la que vimos en la parte de regresión logistica. 
	* Las 3 son fáciles de derivar
* Capas SIN parámetros -> No se entrenan!
	* Son capas que modifican la salida.
	* Esto es lo que veíamos antes de como la regresión logistica es una mezcla de Sig + Regresión.


### Ejecuciones por lotes

Entranamos por lotes. Es más eficiente. Todos calculos de matrices mejor para optimizar.

Es importante también saber cuanto ocupa esto en memoria. En general es: b x n (Tamaño del batch por la cantidad de entradas)


### Diseño de redes

* Dado un problema X
	* ¿Cuántas capas?
	* ¿Cuantas neuronas por capa?
	* ¿Qué funciones de activación?

* No hay un óptimo
	* Varias combinaciones funcionan
	* Lo bueno es que no hay que encontrar la solución óptima, sino una buena solución. Y de esas hay varias.

* ¿Cómo diseñar?
	* Experiencia previa y literatura (Importante porque hay cada vez mas experiencia previa)
	* Búsqueda de hiperparámetros, 
		* #capas, #neuronas, tipos de capa
		* basandonos en la literatura + ir probando


## Aproximación universal 

**Teorema de Cybenko**: Dada una red de 2 capas, fully connected, con activa sigmoidea => podría aproximar cualquier función. Aumentando la cantidad de parametros decremento el error. (Hay variaciones del teorema para otras funciones de activación) 

Explicación e intuición:

	La sigmoidea pero la puedo modular por una linea ( 1/ (! + e ^-(w * x + b)) => Ahí puedo moverla en el eje x, y puedo empinarla/relajar.
	  Con esos dos parámetros puedo cambiar la función y llevar a una zona en particular. Combinando 2 puedo seleccionar areas del gráfico. 
	  (Del minuto 3 al minuto 8 del video).
	  (Hay una explicación con una intuición de dividir a la función en segmentos. Dos neuronas para una región, más regiones mejor aproximo 
	    una función).

De la clase: Cybenko no es constructivo. El asume que tiene los mejores pesos. Básicamente te dice esto es posible, encontra vos la arquitectura y los pesos. Es algo más matematico descriptivo que prágmatico, productivo.


## Backpropagation

* Forward => Entrada -> salida (Almacena valores intermedios)
* Backward => Derivda de salida -> entrada (Utiliza valores intermedios)

Con los valores de entrada, calcula los valores de los enlaces y los guarda. En retroceso, tomando el error, calcula las derivadas y con eso sabe para que lado tiene que mejorar.

El video 4 es buenisimo. Como se aplica backpropagation con los calculos de la derivada para una capa (Lo que me recomendó hacer Dani).
En el video 5 hace la explicación de como funciona backpropagation cuando armó toda la red (Varias capas). 

En las capas, el calculo es modular. Puedo imaginar cada capa como una red en dos sentidos. Cuando hago forward, aplico la función => y = f(X). Cuando hago backward => x = derivar el error.
Modularizando, obtengo capas independientes y las puedo intercambiar. (Antes era más monolítico)

El calculo del error se puede hacer como un calculo de matriz en los batches. Todo resuelto por los frameworks. 


## Evaluación de modelos

Que tan bueno es mi modelo? Como se comporta con datos nuevos. Como lo mido?

* Accuracy hasta ahora. Pero no es una gran métrica.
	* En train
	* En test
	* Situaciones
		* Alto en ambos => todo bien
		* Alto en train => Memorizó - Overfitting
		* Bajo en train => Modelo mal entrenado (Mal el alfa por ejemplo) - Underfitting

Hay que evitar que las redes aprendan los datos de "memoria" (**overfitting**)

Entonces dividimos los datos en train y test. 80-20. Usar el azar. Estratificar con azar. Evitar que haya un orden y diferente grado de representacion de características.

Otra cosa para ver son las **curvas de entrenamiento**. Epocas vs Perdida/Error (Epochs vs Loss). 

### Curvas de entrenamiento

* Nos permite ver como se comporta el error del modelo en cada iteración. 
* No siempre la última iteración tiene el mejor modelo. 
* Podemos ver como se comportan los hiperparametros. 
	* Alpha muy grande, el modelo se comporta erratico
	* Alpha muy chico, el avanza es muy lento
* Keras devuelve el objeto history

### Complejidad del problema vs Complejidad de la solución

Tener un equilibrio entre la complejidad de la solución y la complejidad del problema. 

* Generalización -> Capacidad de un modelo para predecir datos no vistos (sin etiquetar). Se mide con el testing set.
* Overfitting -> Predice bien en train. No generaliza en test.
	* El overfitting se puede ver en las curvas de entrenamiento
	* Cuando las curvas divergen. Train sube y test baja. 
* Underfitting -> Predice mal en train. 


### Hiperparametros

Son los que guían al algoritmo de optimización de nuestro modelo. No hay una configuración ideal, sino que depende del modelo del dataset específico:

* Fx de activación
* Learning rate (alfa)
* Batch size
* # epochs
* Regularización
	* Forma de restringir la complejidad de un modelo.
	* Es útil cuando hay correlación entre features, para filtrar ruido y para evitar overfitting
	* Loss + penalización


### Regularizacion

La regularización es una forma de restringir la complejidad del modelo. Util cuando hay mucha correlación entre features, para filtrar los datos y para evitar el overfitting.

Más grande el coeficiente de regularización, más simple se hace el modelo. Baja la complejidad.

* L1
* L2

### Cross validation

No solo train/test. Agregar validación cruzada con múltiples muestras. 

* Hago K ejecuciones, de forma estratificada, partiendo en k lugares distintos el conjunto de datos.  
	* Muchas veces se reemplaza esto por k ejecución aleatorias independientes.
* Separo en train/test en cada vuelta, y saco una metrica
* Para evaluar, al final de las k iteraciones, saco un promedio de las métricas.

El objetivo es reducir el rol del azar y obtener métricas más robustas.


### Preprocesamiento

Cuando y como debo preprocesar?

* Solo entrenamiento
	* Voy a tener distintos formatos y rangos
* Preprocesar train y test por separado
	* Problemas al tener nuevas distirbuciones
* Preprocesar antes de dividir
	* Contamina train set ( Una forma de hacer trampa )
* **modelo de preprocesamiento**
	* Se genera un modelo de datos con trainset (Por ejemplo media y desviación)
	* Se utiliza el mismo modelo con test
	* Otra opción: El modelo de clasificacion también preprocesa (ConvNets)
	* La ventaja es que se podría imaginar como una capa extra que se encarga del preprocesamiento.

## Uso de memoria de una red

* Base estática
	* El peso de la matriz de _parametros_

* Forward
	* Está capa está multiplicada por el tamaño del batch (Cada variable está multiplicada por b)
		* Ese _b_ es el valor a manipular para lograr que nuestra red entre en memoria
	* Salidas de cada capa (Valores intermedios)
	* Valores necesario para el backward
		* Los X de entrada
		* Los Y verdaderos de esas entradas

* Backward
	* Está capa también está multiplicada por el tamaño del batch.
	* Derivadas de entradas
		* ?
	* Derivadas de parámetros
		* En descenso de gradiente básico => 1 gradiente por cada parámetro
		* En descenso de gradiente avanzado => 1 gradiente + 1 inercia por cada parámetro 
	* Valores intermedios
		* 1 valor por cada gradiente intermedio

**Forward y backward los condicionas el tamaño del lote**. Ahí probablemente tengo que ajustar b a mi cantidad de memoria. 


 
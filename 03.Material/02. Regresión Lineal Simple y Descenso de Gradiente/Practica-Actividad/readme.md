# Prubas

## Aprendizaje

* 00
	* m = 5
	* b = -4
	* max_iteraciones = 400
	* alpha = 0.0001
* 01
	* m = -5
	* b = -4
	* max_iteraciones = 400
	* alpha = 0.0001
* 02
	* m = -5
	* b = -4
	* max_iteraciones = 1000
	* alpha = 0.001
* 03
	* m = -5
	* b = -4
	* max_iteraciones = 4000
	* alpha = 0.001
* 04
	* m = -5
	* b = -4
	* max_iteraciones = 2000
	* alpha = 0.001
* 05
	* m = 0
	* b = -4
	* max_iteraciones = 2000
	* alpha = 0.001

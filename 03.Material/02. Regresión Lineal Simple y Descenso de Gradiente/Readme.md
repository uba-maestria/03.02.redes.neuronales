# Modelo de regresión lineal

El modelo más simple. Asume una relación lineal.  

* X
* Y
* Y = m * X + b
* E = Error cuadratico = ( y_i - f(x_i) ) ^ 2
	* Es al cuadrado => 
		* Penaliza los errores más grandes (Penaliza pero es la mas simple de las potencias que penalizan los errores). 
		* Es derivable.
	* Error promedio para calcular
	* El error cuadratico promedio tiene forma de parábola. Uno busca el punto donde se minimiza (Segunda derivada?)


## Forma de la función de error

Es una parábola (Video 2 prueba de que es cuadrática)


## Optimización de funciones

Aprendizaje supervisado. 

Tenemos valores de entrada (x_i) y valores de salida (y_i). Buscamos una función que explique la salida producida, a partir de los valores de entradas. 

El proceso de aprendizaje supervisada va a ser encontrar los _parámetros óptimos_ para esa función, que minimicen el error entre salida esperada y salida real.

	El proceso de aprendizaje es la optimización del error.

Optimizar es buscar algún mínimo, variando sus parámetros. 

Min(f) = Max(-f) => Puedo hacer uno u otro.

### Clasificacion de las funciones

* Convexa
	* Único mínimo (Mínimo global)
	* E para regresión lineal es siempre convexa
* No-convexa
	* Varios mínimos (Uno global y varios locales)
	* E para redes es no-convexa

### Dificultades

Más de 2 parametros hay que tener un algoritmo para optimizar. No podemos visualizar, ni podemos probar todas las combinaciones en la práctica.

### Algoritmos de optimizacion

* Generales
	* Pocas asunciones sobre la función
	* Mayor tiempo de ejecución
	* Pocas garantías
	* Ejemplos: Fuerza bruta (Pruebo todos los valores), búsqueda aleatoria (Grid search, random search)
	* Dificultades:
		* Costo computacional
		* Criterio de convergencia, cuando termino?
		* Como evaluo la función de error?
		* Multiples minimos locales, 

* Especializados
	* No siempre existen o son aplicables
	* Ejemplo: Camino mínimo en un grafo (Hay algoritmos específicos

## Descenso de gradiente

### Características

* Iterativo
* Generalizable
	* Regresión Lineal
	* Regresión Logística
	* Redes Neuronales
	* Máquinas de Vectores de Soporte
	* Cualquier modelo con función de error E derivable
* Escalable (con modificaciones)

Es un algoritmo intermedio. El más utilizado para redes neuronales. No es especializado, pero tampoco es 100% generalista. Hace la asunción de que la función es derivable 


### Derivable 

* Si es suave, si no tiene interrupciones, o interrupciones fuertes por lo menos. Continua y suave. 
* Pendiente de la recta tangente en el punto X. Es la taza de cambio de dy/dx
* El signo indica la dirección de crecimiento (Alrededor del punto)


### Calculo/Uso

Necesitamos:

* Necesitamos una función de error (E)
* Parametro w 
	* Valor inicial => w = w_0 aleatorio
* Derivada del error
* Velocidad de aprendizaje $/alpha$
	* Tamaño del paso

Empieza con un valor aleatorio y calcula el gradiente (O derivada) y con eso ve en que dirección moverse del mínimo. Itero hasta converger.


### Tamaño del alfa

* α demasiado chico
	* Poco avance por iteración
	* Alto costo computacional
* α “correcto”
* α demasiado grande
	* Saltos grandes
	* Puede no converger
	* Errores numéricos

### Funciones no convexas

En redes neuronales buscamos el gradiente de una función no convexas. 

_Posible problemas_: 
* La solución varía según varían los parametros iniciales (w)
* Va a un mínimo local (Posiblemente global pero que no se sabe con certeza)


### Regresión lineal 

#### Con MLE

MLE => Maximum likelihood estimation (Método estadístico)

Asumo y = f(x) + error = m * x + b + error

* error se distribuye como una normal(0, sigma)
* m, b y sigma son parametros
	* Busco los estimadores m', b', sigma'
* MLE es un enfoque clásico, busco una solicón análitica, es simple
	* Como desventaja no escala bien. Muchos datos se demora mucho.

#### Con Descenso de gradiente

Ecuaciónes, derivadas e iteraciones.

* Normalizando la variables converge más rápido.
	* Diferentes escalas afecta también
* Los valores iniciales afectan a la optimización
	* Podemos aprovechar el conocimiento de dominio para mejorar el proceso


### ¿Cuándo terminar?

* Iteraciones fijas
* Tolerancia de error

¿Cómo determinar?

* Monitorear parámetros vs error
* Ver si se llega al mínimo
* No se puede visualizar E con n>2 parám.


#### Curvas de error

Monitorear Error vs Iteración

* No requiere 1D o 2D
* Funciona si el error no es convexo

Cada Iteración
* Calcular error (Cada cierta cantidad de iteraciones o epochs. Depende de la velocidad de iteración)
* Actualizar gráfico
* Guardar en vector y dibujar al final => Para el proceso
* Guardar en un archivo => Si quiero continuar despues

Sirve para otros métodos iterativos.

En las curvas podemos diagnosticar tambien:

* Convergencia Rápida
* Ajustes constantes -> Avanza pero muy lento. Puede ser un alfa muy chico o que faltan iteraciones.
* Divergencia -> Alfa muy alto. No haber normalizado. La curva no converge e incluso se dispara.
* Tasa de error constante -> El modelo no aprende. Problema muy complejo, alfa muy chico.

En las curvas de train/test:

* Si ambas convergen => Buen entrenamiento y buena generalizacion
* Train converge, y test con error alto => Mala generalización
* Convergen, y luego divergen => Overfitting/Sobre entrenamiento
	* Early stopping
	* Dos ideas:
		* Entrenar hasta el final, pero guardar checkpoints del modelo. Elegir el checkpoint con menor error en el conjunto de prueba
		* Esperar que el error sea menor a cierto valor (O que el promedio, o un promedio ponderado, o alguna transformación del error en las últimas n iteraciones)
* Ambas no convergen. Error constante => Cambiar el modelo.


### Métricas de error

* Función de error E
	* Diseñada para optimizar
	* Por ejemplo MSE (Error de cuadrados mínimos). Funciona para otimizar, cumple con las premisas matemáticas, pero para explicar es dificil.
* Métricas
	* Similares a las funciones de error
	* No tienen que ser optimizables
	* Más fáciles de interpretar
	* Fáciles de comparar entre modelos
	* Ej:
		* MAE => Mean absolute error. Para problemas de regresión. No es taaan útil para comparar entre modelos, no tiene un sentido sobre el problema.
		* 
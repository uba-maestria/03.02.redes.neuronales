# Clase 1

## Tipos de datos

* Escalar (Tensor OD)
* Vector (Tensor 1D)
* Matriz (Tensor 2D)
* Tensor 3D (Cubo de rubik)
* Tensor n-Dimensiones

## Escalas

* Numeros - bajo nivel
* Tensores - nivel medio
* Capas - nivel alto

Hoy ya hablamos solo de capas y usamos librerias mucho más resueltas.

## Topologias

Distintas arquitecturas. Podríamos conectarlas como quisieramos a priori (Mientras que sean compatibles entradas y salidas)

* Lineal / Secuencial
* Aciclica
* Ciclica / Recurrente


(Notas de clases. Hay más y mejor info en: ~/Maestria/03.02.Redes.Neuronales/03.02.redes.neuronales/03.Material/01. Introducción/1. Redes Neuronales Modernas.md)
# 03.02.Redes.Neuronales

## Clases

La primera clase es este miércoles 23/3 a las 19hs. Por ser la primera clase, será dictada de forma presencial en la UBA, en el aula 1105 del nuevo edificio Zero+Infinito (queda a lado del pabellón I). 

**Inicio de clases**: Miércoles 23/3, 19hs, Presencial en la UBA.
**Modalidad**: A distancia, salvo por los exámenes y la primera clase que son presenciales.
**Duración**: 2 meses

### Encuentros sincrónicos

Miércoles y Viernes de 19hs a 22hs por Zoom. En cada encuentro discutiremos el material, las autoevaluaciones y el resultado de las actividades. Los encuentros se graban y suben a una carpeta compartida.

El resto de las clases será a distancia, con Zoom, salvo por las últimas dos clases donde tomarán el examen final del curso.

	Redes Neuronales - Miércoles y Viernes de 19 a 22 hs.
	https://exactas-uba.zoom.us/j/88667775001
	ID de reunión: 886 6777 5001
	Código de acceso: rn22-olep


## Requerimientos

Durante la cursada, vamos a trabajar con Python 3, y con las librerías Tensorflow y Keras. Tenemos algunas guías de instalación para Windows y Linux (https://drive.google.com/drive/folders/1eyUwMg6YW7PqhWXeyg7QKJk_koiZbuqH)

También será posible usar Google Colab en algunos ejercicios, pero recomendamos que instalen un entorno local por la simplicidad para acceder a conjuntos de datos y resultados.


## Docentes

* Dr. Waldo Hasperué
* Dr. Facundo Quiroga


## Comunicación y Foros de discusión

Los foros están disponibles para discutir de manera asincrónica sobre los temas y las actividades. Siéntanse libres de publicar y comentar para enriquecer el desarrollo del curso.

Para consultas administrativas personales (no del contenido o las actividades del curso) pueden utilizar nuestro correo de la materia (**redesneuronalesuba@gmail.com**).

Para otras consultas, no relacionadas a la materia, pueden utilizar nuestro correo personal:

* Waldo -> //whasperue@lidi.info.unlp.edu.ar
* Facundo -> //fquiroga@lidi.info.unlp.edu.ar

## Utils

* Teoricas y actividades de cada semana -> http://157.92.26.246/campus/course/view.php?id=8
* Reglamento y Cronograma -> https://drive.google.com/file/d/1W2GWLHJWwPFVtLYOt43B5ruCIuU39Oxw/view
* Grabaciones de los encuentros -> https://drive.google.com/drive/folders/1EOLAbu1QaPVXKvEnrEA31-ZtpmfcNcZR
* Programa tentativo (Página de la maestría) -> http://datamining.dc.uba.ar/datamining/index.php/programa-de-estudios/86-academico/299-redesneuronales